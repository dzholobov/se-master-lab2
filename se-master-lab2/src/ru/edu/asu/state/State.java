package ru.edu.asu.state;

interface State {
	void insertQuarter(VendingMachine machine);

	void ejectQuarter(VendingMachine gumballMachine);

	void dispense(VendingMachine gumballMachine);


}
