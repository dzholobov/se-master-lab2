package ru.edu.asu.state;

public class VendingMachine {
 
	final static int SOLD_OUT = 0;
	final static int NO_COIN = 1;
	final static int HAS_COIN = 2;
	final static int SOLD = 3;
 
	int state = SOLD_OUT;
	int count = 0;
  
	public VendingMachine(int count) {
		this.count = count;
		if (count > 0) {
			state = NO_COIN;
		}
	}
  
	public void insertCoin() {
		if (state == HAS_COIN) {
			System.out.println("������ ��������� ������ ����� ������");
		} else if (state == NO_COIN) {
			state = HAS_COIN;
			System.out.println("�� ��������� ������");
		} else if (state == SOLD_OUT) {
			System.out.println("������ ��������� ������, ��� ������ ����������");
		} else if (state == SOLD) {
        	System.out.println("���������, ����� ��������");
		}
	}

	public void ejectCoin() {
		if (state == HAS_COIN) {
			System.out.println("������ ����������");
			state = NO_COIN;
		} else if (state == NO_COIN) {
			System.out.println("�� �� ��������� ������");
		} else if (state == SOLD) {
			System.out.println("�� ��� ������� �����, ������ ����������");
		} else if (state == SOLD_OUT) {
        	System.out.println("���������� �������, ������ ��� �� ��������");
		}
	}
 

 
 
	public void pushSelectButton() {
		if (state == SOLD) {
			System.out.println("������� �������");
		} else if (state == NO_COIN) {
			System.out.println("�� ��������� ������� �����, �� �������� ������");
		} else if (state == SOLD_OUT) {
			System.out.println("��������� ������ �����������");
		} else if (state == HAS_COIN) {
			System.out.println("����� ������...");
			state = SOLD;
			dispense();
		}
	}
 
	public void dispense() {
		if (state == SOLD) {
			System.out.println("����� ��������...");
			count = count - 1;
			if (count == 0) {
				System.out.println("������ �����������!");
				state = SOLD_OUT;
			} else {
				state = NO_COIN;
			}
		} else if (state == NO_COIN) {
			System.out.println("������ ���������� ���������� ������");
		} else if (state == SOLD_OUT) {
			System.out.println("����� �� �����");
		} else if (state == HAS_COIN) {
			System.out.println("����� �� �����");
		}
	}
 
	public void refill(int numOfProducts) {
		this.count = numOfProducts;
		state = NO_COIN;
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("\n�������� �������");
		result.append("� �������: " + count + " �������");
		result.append("\n���������: ");
		if (state == SOLD_OUT) {
			result.append("������ ����������");
		} else if (state == NO_COIN) {
			result.append("�������� ������");
		} else if (state == HAS_COIN) {
			result.append("�������� ������ ������");
		} else if (state == SOLD) {
			result.append("����� ��������");
		}
		result.append("\n");
		return result.toString();
	}
}


