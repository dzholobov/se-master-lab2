package ru.edu.asu.state;

public class NoCoinState implements State {

	@Override
	public void insertQuarter(VendingMachine machine) {
		machine.state = VendingMachine.HAS_COIN;
		System.out.println("�� ��������� ������");
	}

	@Override
	public void ejectQuarter(VendingMachine vendingMachine) {
		System.out.println("�� �� ��������� ������");
	}

	@Override
	public void dispense(VendingMachine vendingMachine) {

		System.out.println("������ ���������� ���������� ������");
	}

}
