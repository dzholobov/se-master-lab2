package ru.edu.asu.builder;

public interface NodeConverter {
	
	public void readProject(String projectName);
	public void readSupervisor(String supervisorName);
	public void readEmail(String email);
	public void readDescription(String description);
	

}
