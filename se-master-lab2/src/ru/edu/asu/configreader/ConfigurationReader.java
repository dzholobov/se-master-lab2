package ru.edu.asu.configreader;

import ru.edu.asu.configreader.xml.XMLBuildConfigurationReader;
import ru.edu.asu.configreader.yaml.YamlBuildConfigurationReader;

public class ConfigurationReader {

	
	
	public static void main(String[] args) {
		XMLBuildConfigurationReader buildConfigurationReader = new XMLBuildConfigurationReader("build.xml");
		System.out.println("XML Build config: "+buildConfigurationReader.getProject());
		YamlBuildConfigurationReader yamlConfigReader = new YamlBuildConfigurationReader("build.yaml");
		System.out.println("Yaml build config: "+yamlConfigReader.getBuildConfig());
	}
}
